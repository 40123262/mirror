﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

//[RequireComponent(typeof(Rigidbody))]
public class LocalPlayerMovement : NetworkBehaviour
{
    Vector2 movement;
    public float Speed;
  //  Rigidbody body;
    // Start is called before the first frame update
    void Start()
    {
     //   body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;
        
            GetInput();
            Move();
        
    }
    private void GetInput()
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");
    }
    private void Move()
    {
        if(movement.magnitude>0)
        {
            var posDelta = new Vector3(movement.x, 0, movement.y);
            transform.position += posDelta * Speed * Time.deltaTime;
          //  body.velocity = posDelta * Speed;
        }
    }
}
